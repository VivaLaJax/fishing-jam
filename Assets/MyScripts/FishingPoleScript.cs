using UnityEngine;
using System.Collections;

public class FishingPoleScript : MonoBehaviour 
{
	public Texture reticleTexture;
	public Texture arrowTexture;
	public Texture barTexture;
	public Texture breakTexture;
	public Material ropeMaterial;
	private Ray ray;
    private RaycastHit hit;
	private GameObject target;
	public float leftBound;
	public float rightBound;
	public float currPos;
	public float boundary;
	public int speed;
	
	private float midSection;
	private float leftSection;
	private float rightSection;
	
	private bool dir;
	
	public int maxBreaks;
	public int numOfBreaks;
	
	private LineRenderer lineRend;
	public GameObject rodend;
	public GameObject fishingLine;
	public bool fishing;
	
	void Start () 
	{
		maxBreaks = 3;
		midSection = 25f;
		leftSection = 39f;
		rightSection = 39f;
		speed = 8;
		boundary = 9.0f;
		leftBound = Screen.width/2 - barTexture.width/2 - arrowTexture.width/2 + boundary;
		rightBound = Screen.width/2 + barTexture.width/2 - arrowTexture.width/2 - boundary;
		Screen.showCursor = false;
		dir = true;
	}
	
	void Update () 
	{			
		if(Input.GetMouseButtonDown(1))
		{			
			fishing = false;
			this.transform.parent.GetComponent<PlayerMovementScript>().inFishing(false);
			
			Ray screenray = Camera.main.ScreenPointToRay(Input.mousePosition);
			
			ray = new Ray(rodend.transform.position,screenray.direction);
			
			//this if checks, a detection of hit in an GameObject with the mouse on screen
      		if(Physics.Raycast(ray, out hit))
      		{				
				Debug.DrawLine(ray.origin, hit.point, Color.blue);
         		//GameObject.Find("Nameofyourobject") search your gameobject on the hierarchy with the desired name and allows you to use it
         		GameObject clickable = (GameObject) hit.transform.gameObject;
				
				if(!(target==null))
				{
					if(!(fishingLine==null))
					{
						Destroy(fishingLine);
					}
					if(target.GetComponent<selectScript>())
					{
						target.GetComponent<selectScript>().Deselected();
						target = null;
					}
				}
				
				if(clickable.tag=="Fishable")
				{
					fishingLine = new GameObject("Fishing Line");
					fishingLine.AddComponent("LineRenderer");
					lineRend = fishingLine.GetComponent<LineRenderer>();
					
					lineRend.SetVertexCount(2);
					lineRend.SetPosition(0,rodend.transform.position);
					lineRend.SetPosition(1,hit.transform.position);
					
					lineRend.castShadows = true;
					lineRend.useWorldSpace = true;
					
					lineRend.material = ropeMaterial;
					
					lineRend.SetWidth(0.2f,0.2f);
					
					if(clickable.GetComponent<selectScript>())
					{
						clickable.GetComponent<selectScript>().Selected(this.gameObject);
						target = clickable;
						
						fishing = true;
						numOfBreaks = 0;
						currPos = leftBound;
						this.transform.parent.GetComponent<PlayerMovementScript>().inFishing(true);
					}
				}
			}
		}
		
		if(fishing)
		{
			if(dir)
			{
				currPos+=speed;
				if(currPos>=rightBound)
				{
					dir=false;
				}
			}
			else
			{
				currPos-=speed;
				if(currPos<=leftBound)
				{
					dir=true;
				}
			}
			
			if(Input.GetButtonDown("Jump"))
			{
				if(checkBar())
				{
					target.GetComponent<selectScript>().HasTugged();
					lineRend.SetPosition(1,hit.transform.position);
				}
				else
				{
					//add a break
					numOfBreaks++;
					if(numOfBreaks==maxBreaks)
					{
						if(target.GetComponent<selectScript>())
						{
							target.GetComponent<selectScript>().Deselected();
							target = null;
						}
						fishing = false;
						this.transform.parent.GetComponent<PlayerMovementScript>().inFishing(false);
						if(!(fishingLine==null))
						{
							Destroy(fishingLine);
						}
						
						GameObject.Find("StoryObject").GetComponent<StoryScript>().setTo(3);
						GameObject.Find("StoryObject").GetComponent<StoryScript>().playNext();
					}
				}
			}
		}
	}
	
	void OnGUI()
	{
		if(fishing)
		{
			//other
			GUI.DrawTexture(new Rect(Screen.width/2-barTexture.width/2,Screen.height-(barTexture.height+barTexture.height/2),barTexture.width,barTexture.height),barTexture);
			GUI.DrawTexture(new Rect(currPos,Screen.height-(barTexture.height),arrowTexture.width,arrowTexture.height),arrowTexture);
			for(int i=0; i<numOfBreaks; i++)
			{
				GUI.DrawTexture(new Rect((Screen.width/2-breakTexture.width)+(i*breakTexture.width),Screen.height-(3*barTexture.height/2 + breakTexture.height),breakTexture.width,breakTexture.height),breakTexture);
			}
		}
		else
		{
			GUI.DrawTexture(new Rect(Input.mousePosition.x-reticleTexture.width/2, (Screen.height-(Input.mousePosition.y))-reticleTexture.height/2, reticleTexture.width, reticleTexture.height),reticleTexture);
		}
	}
	
	bool checkBar()
	{
		bool retVal = false;
		//first
		if(currPos<=((leftBound+boundary)+leftSection))
		{
			retVal = true;
		}
		//second
		else if(currPos>=(Screen.width/2-arrowTexture.width/2-midSection) && currPos<=(Screen.width/2-arrowTexture.width/2+midSection))
		{
			retVal = true;
		}
		//third
		else if(currPos>=((rightBound-boundary)-rightSection))
		{
			retVal = true;
		}
		
		return retVal;
	}
}
