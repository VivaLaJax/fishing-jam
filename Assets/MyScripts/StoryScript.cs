using UnityEngine;
using System.Collections;

public class StoryScript : MonoBehaviour 
{
	string[] first = {"She always had quite the menagerie.","Wonderful creatures they were."};
	string[] second = {"Whenever I asked her where \n she'd met her friends,","She would always talk about \n how she could find more things to care for \n by taking the time to notice what was around her.", "I admired her for that."};
	string[] third = {"Though she had no reason to rush,","sometimes, she would say,","It is good to catch the big fish."};
	string lineBreak = "Sometimes her line would break.";
	public int index = 0;
	public bool showing = false;
	public int which = 0;
	public float max = 3f;
	public float timer;
	
	void Start()
	{
		timer = 0;
	}
	
	void Update()
	{
		if(showing)
		{		
			if(timer<=0)
			{
				switch(which)
				{
					case 0:
					{				
						if(index==first.Length)
						{
							showing=false;
							index = 0;
							which++;
							CamText.text="";
						}
						else
						{
							CamText.text = first[index++];
						}
					
						timer = max;
						break;
					}
					case 1:
					{				
						if(index==second.Length)
						{
							showing=false;
							index = 0;
							which++;
							CamText.text="";
						}
						else
						{
							CamText.text = second[index++];
						}
					
						timer = max;
						break;
					}
					case 2:
					{
						if(index==third.Length)
						{
							showing=false;
							index = 0;
							which++;
							CamText.text="";
						}
						else
						{
							CamText.text = third[index++];
						}
					
						timer = max;
						break;
					}
					case 3:
					{
						if(index==1)
						{
							showing=false;
							index = 0;
							which++;
							CamText.text="";
						}
						else
						{
							CamText.text = lineBreak;
							index++;
						}
					
						timer = max;
						break;
					}
				}
			}
			else
			{
				timer-=Time.deltaTime;
			}
		}
	}
	
	public void playNext()
	{
		showing = true;
	}
	
	public void setTo(int who)
	{
		which = who;
	}
}
