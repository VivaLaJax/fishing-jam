using UnityEngine;
using System.Collections;

public class TuggingScript : MonoBehaviour 
{
	public int numOfTugs;
	public int maxTugs;
	public GameObject selectedBy;
	public GameObject island;
	private Vector3 moveDirection = Vector3.zero;
	private float dist;
	
	void Start () 
	{
		maxTugSetup();
		numOfTugs = maxTugs;
	}
	
	void Update () 
	{
		if(selectedBy != null)
		{
			Debug.DrawLine(transform.position,selectedBy.transform.position);
		}
	}
	
	public void SetSelected(GameObject sel)
	{
		selectedBy = sel;
		if(selectedBy==null)
		{
			this.gameObject.GetComponent<MovementScript>().enabled = true;
			numOfTugs = maxTugs;
		}
		else
		{
			this.gameObject.GetComponent<MovementScript>().enabled = false;
			transform.LookAt(selectedBy.transform.position);
			dist = Vector3.Distance(transform.position,selectedBy.transform.position + 30*(selectedBy.transform.forward+selectedBy.transform.up));
		}
	}
	
	public void Tug()
	{
		if(numOfTugs>0)
		{
			moveDirection = transform.forward;
	  	
			moveDirection *= dist/maxTugs;
			
			transform.position +=moveDirection;
		
			numOfTugs--;
		}
		else if(numOfTugs==0)
		{
			//captured
			Destroy(GameObject.Find("Fishing Line"));
			GameStartScript start = GameObject.FindGameObjectWithTag("GameStart").GetComponent<GameStartScript>();
			
			if(this.gameObject.name.Equals("Giant Fish(Clone)"))
			{
				Destroy(this.gameObject);
				start.giantFishGet();
			}
			else
			{
				Screen.showCursor = false;
				PlayerMovementScript script = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovementScript>();
				script.enabled = true;
				script.inFishing(false);
				GameObject.FindGameObjectWithTag("Rod").GetComponent<FishingPoleScript>().fishing = false;
				
				GameObject slot1 = GameObject.Find("slotOne");
				GameObject slot2 = GameObject.Find("slotTwo");
				
				if(!slot1.GetComponent<slotCheck>().getFull())
				{		
					GameObject.Find("StoryObject").GetComponent<StoryScript>().setTo(0);
					GameObject.Find("StoryObject").GetComponent<StoryScript>().playNext();
					
					this.gameObject.transform.position = slot1.transform.position;
					this.gameObject.transform.rotation = Quaternion.identity;
					this.gameObject.transform.parent = slot1.transform;
					slot1.GetComponent<slotCheck>().setFull(true);
				}
				else
				{
					GameObject.Find("StoryObject").GetComponent<StoryScript>().setTo(1);
					GameObject.Find("StoryObject").GetComponent<StoryScript>().playNext();
					
					this.gameObject.transform.position = slot2.transform.position;
					this.gameObject.transform.rotation = Quaternion.identity;
					this.gameObject.transform.parent = slot2.transform;
					slot2.GetComponent<slotCheck>().setFull(true);
				}
			}
		}
	}
	
	public int GetTugs()
	{
		return numOfTugs;
	}
	
	private void maxTugSetup()
	{
		string objName = this.gameObject.name;
		
		switch(objName)
		{
			case "Giant Fish(Clone)":
			{
				maxTugs = 5;
				break;
			}
			case "Dragon":
			{
				maxTugs = 3;
				break;
			}
			case "Bird":
			{
				maxTugs = 2;
				break;
			}
			default:
			{
				maxTugs = 3;
				break;
			}
		}
	}
}
