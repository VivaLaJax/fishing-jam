using UnityEngine;
using System.Collections;

public class SelectMenu : MonoBehaviour 
{
	void OnGUI()
	{
		if (GUILayout.Button("Level 1 - Click to select and move"))
		{
			Application.LoadLevel("Level_1");
		}
		if (GUILayout.Button("Level 2 - Line of Sight"))
		{
			Application.LoadLevel("Level_2");
		}
		if (GUILayout.Button("Level 3 - Drag to Select"))
		{
			Application.LoadLevel("Level_3");
		}
		if (GUILayout.Button("Level 4 - Instantiating a level"))
		{
			Application.LoadLevel("Level_4");
		}
		if (GUILayout.Button("Level 5 - Targetting And Attacking (Unfinished)"))
		{
			Application.LoadLevel("Level_5");
		}
		if (GUILayout.Button("Level 6 - A* Path Finding"))
		{
			Application.LoadLevel("Level_6");
		}
		if (GUILayout.Button("Level 7 - Tactical RPG Battle"))
		{
			Application.LoadLevel("Level_7");
		}
	}
}
