using UnityEngine;
using System.Collections;

public class clickToTarget : MonoBehaviour 
{
	private Ray ray;
    private RaycastHit hit;
	private GameObject target;
	private Vector3 walkTo;
	private Transform myTransform;
	private int attackRange;
	
	// Use this for initialization
	void Start () 
	{
		attackRange = 5;
		myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () 
	{
		ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		
   		//this if check for the mouse left click
   		if (Input.GetMouseButton(0)) 
   		{
      		//this if checks, a detection of hit in an GameObject with the mouse on screen
      		if(Physics.Raycast(ray, out hit, 20f))
      		{
				Debug.DrawRay(ray.origin, hit.point, Color.blue);
         		//GameObject.Find("Nameofyourobject") search your gameobject on the hierarchy with the desired name and allows you to use it
         		GameObject clickable = (GameObject) hit.collider.gameObject;
				
				if(clickable.tag!="Player")
				{
					if(!(target==null))
					{
						if(target.GetComponent<selectScript>())
						{
							target.GetComponent<selectScript>().Deselected();
							target = null;
						}
					}
				}
				else if(clickable.tag=="Player")
				{
					//Destroy(clickable);
					if(clickable.GetComponent<selectScript>())
					{
						//clickable.GetComponent<selectScript>().Selected();
						target = clickable;
					}
				}					
		    }
   		}
		
		if (Input.GetMouseButton(1))
		{
			if(!(target==null))
			{
				if(Physics.Raycast(ray, out hit, 20f))
      			{
					Debug.DrawRay(ray.origin, hit.point, Color.blue);
					
					GameObject clickable = (GameObject) hit.collider.gameObject;
				
					if(clickable.tag=="Unit")
					{
						Vector3 moveTo = new Vector3(hit.point.x-attackRange, target.transform.position.y, hit.point.z-attackRange);
						StartCoroutine(MoveObject(target.transform, target.transform.position, moveTo, 2.0f));
					}
					else
					{
						Vector3 moveTo = new Vector3(hit.point.x, target.transform.position.y, hit.point.z);
						StartCoroutine(MoveObject(target.transform, target.transform.position, moveTo, 2.0f));
					}
				}
			}
		}
		
		//up (height for mouse, 0 for screen)
		if(Input.mousePosition.y>=Screen.height)
		{
			myTransform.position+= new Vector3(0,0,1);
		}
		
		//right
		if(Input.mousePosition.x>=Screen.width-20)
		{
			myTransform.position+= new Vector3(1,0,0);
		}
		
		//left
		if(Input.mousePosition.x<=20)
		{
			myTransform.position-= new Vector3(1,0,0);
		}
		
		//down (0 for mouse, height for screen)
		if(Input.mousePosition.y<=0)
		{
			myTransform.position-= new Vector3(0,0,1);
		}
	}
	
	IEnumerator MoveObject (Transform thisTransform, Vector3 startPos, Vector3 endPos, float time) 
	{
    	float i = 0.0f;
    	float rate = 1.0f/time;
    	while (i < 1.0f) 
		{
        	i += Time.deltaTime*rate;
        	thisTransform.position = Vector3.Lerp(startPos, endPos, i);
			yield return 0;
 		}
	}
}
