using UnityEngine;
using System.Collections;

public class slotCheck : MonoBehaviour 
{
	public bool slotFull = false;
	
	public void setFull(bool slot)
	{
		slotFull = slot;
	}
	
	public bool getFull()
	{
		return slotFull;
	}
}
