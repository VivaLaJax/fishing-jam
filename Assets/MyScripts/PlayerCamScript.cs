using UnityEngine;
using System.Collections;

public class PlayerCamScript : MonoBehaviour 
{
	void Awake ()
	{
		if(networkView.isMine)
		{
    		camera.enabled = true;
    	}
		else
		{
    		camera.enabled = false;
    	}
	}
}
