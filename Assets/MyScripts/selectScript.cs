using UnityEngine;
using System.Collections;

public class selectScript : MonoBehaviour 
{
	bool selected;
	Transform me;
	TuggingScript tugScript;
	
	// Use this for initialization
	void Start () 
	{
		selected = false;
		me = transform;
		tugScript = this.gameObject.GetComponent<TuggingScript>();
	}
	
	public void Selected(GameObject sel)
	{
		tugScript.SetSelected(sel);
		selected = true;
		me.renderer.material.color = Color.blue;
	}
	
	public void Deselected()
	{
		tugScript.SetSelected(null);
		selected = false;
		me.renderer.material.color = Color.gray;
	}
	
	public int HasTugged()
	{
		this.gameObject.GetComponent<TuggingScript>().Tug();
		
		return tugScript.GetTugs();
	}
}
