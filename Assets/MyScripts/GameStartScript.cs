using UnityEngine;
using System.Collections;

public class GameStartScript : MonoBehaviour 
{
	public int numberOfFishies;
	private bool gameWon = false;
	private bool keptOn = false;
	public bool flyby = true;
	public GameObject fish;
	public GameObject cam;
	public GameObject player;
	public float dist;
	public int speed;
	private int storyIndex;
	public GameObject spawn;
	private string[] story = {"Once, a long time ago","I met a little girl.","The most patient, content person I have ever met.","And she liked to fish."};
	void Start () 
	{
		Vector3 spawnpoint;
		Vector3 terrainSize = Terrain.activeTerrain.terrainData.size;

		for(int i=0; i<numberOfFishies; i++)
		{
			spawnpoint = new Vector3(Random.Range(0,terrainSize.x),Random.Range(terrainSize.y,500),Random.Range(0,terrainSize.z));
			
			Instantiate(fish,spawnpoint,Quaternion.identity);
		}
		
		Instantiate(player,spawn.transform.position,Quaternion.identity);
		player = GameObject.FindGameObjectWithTag("Player");
		speed = 10;
		storyIndex = 0;
		
		CamText.text = story[storyIndex];
	}
	
	void Update()
	{
		if(flyby)
		{
			cam.transform.position += speed*(cam.transform.forward*Time.deltaTime);
			speed++;
			
			dist = (player.transform.position-cam.transform.position).sqrMagnitude;
			
			for(int i=1; i<story.Length; i++)
			{
				if(dist <= (player.transform.position * (1f-i*(1f/story.Length))).sqrMagnitude && storyIndex<i)
				{
					storyIndex++;
					CamText.text = story[storyIndex];
				}
			}			
			
			if(dist <= (player.transform.position * 0.01f).sqrMagnitude || Input.GetMouseButtonDown(0))
			{
				CamText.text = "";
				Destroy(cam);
				flyby = false;
				player.GetComponentInChildren<Camera>().enabled = true;
			}
		}
	}
	
	public void giantFishGet()
	{
		//game end
		gameWon = true;
		GameObject.Find("StoryObject").GetComponent<StoryScript>().setTo(2);
		GameObject.Find("StoryObject").GetComponent<StoryScript>().playNext();
	}
	
	void OnGUI()
	{
		if(gameWon)
		{
			Screen.showCursor = true;
			GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovementScript>().enabled = false;
			GameObject.FindGameObjectWithTag("Rod").GetComponent<FishingPoleScript>().fishing = false;
			if (GUILayout.Button("Restart"))
			{
				Debug.Log("Restarted");
				Application.LoadLevel("Level_1");
				//gameWon = false;
			}
			if (GUILayout.Button("Continue"))
			{
				//keptOn = true;
				Screen.showCursor = false;
				PlayerMovementScript script = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovementScript>();
				script.enabled = true;
				script.inFishing(false);
				GameObject.FindGameObjectWithTag("Rod").GetComponent<FishingPoleScript>().fishing = false;
				gameWon = false;
			}
		}
	}
}
