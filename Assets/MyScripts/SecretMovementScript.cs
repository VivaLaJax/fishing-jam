using UnityEngine;
using System.Collections;

public class SecretMovementScript : MovementScript
{
	Vector3 moveDirection;
	public GameObject myRotatepoint;
	
	void Start () 
	{
		speed = 10;
	}

	void Update () 
	{
        transform.RotateAround(myRotatepoint.transform.position,Vector3.up,20 * Time.deltaTime);
	}
}
