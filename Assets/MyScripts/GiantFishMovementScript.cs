using UnityEngine;
using System.Collections;

public class GiantFishMovementScript : MovementScript
{
	private bool arrived = true;
	private Vector3 moveDirection = Vector3.zero;
	private Vector3 endpoint = Vector3.zero;
	private Vector3 terrainSize;
	
	void Start () 
	{
		terrainSize = Terrain.activeTerrain.terrainData.size;
	}
	
	void OnEnable()
	{
		transform.LookAt(endpoint);
	}
	
	void Update () 
	{
		if(arrived)
		{
			arrived = false;
		
			//choose a new end point
			endpoint = new Vector3(Random.Range(0,terrainSize.x),Random.Range(terrainSize.y,500),Random.Range(0,terrainSize.z));
		
			//rotate towards the point
			transform.LookAt(endpoint);
		}
		else
		{
			moveDirection = transform.forward;
	       	//moveDirection = transform.TransformDirection(moveDirection);
			moveDirection *= speed;
			//moveDirection.y = riseSpeed;
		
			transform.position +=(moveDirection * Time.deltaTime);
		
			//check to see if we've arrived
			if((endpoint-transform.position).sqrMagnitude <= (endpoint * 0.01f).sqrMagnitude)
			{
				arrived = true;
			}
		}
		Debug.DrawLine(transform.position,endpoint);
	}
}
