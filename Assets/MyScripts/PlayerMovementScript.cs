using UnityEngine;
using System.Collections;

public class PlayerMovementScript : MonoBehaviour 
{
	public int speed = 7;
	public int gravity = 20;
	public int jumpSpeed = 10;
	public bool fishing = false;
	private Vector3 moveDirection = Vector3.zero;
	private CharacterController controller;
	
	// Use this for initialization
	void Start () {
		controller=GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//controller.Move(new Vector3(Input.GetAxis("Horizontal")*-speed*Time.deltaTime, -gravity * Time.deltaTime, Input.GetAxis("Vertical")*-speed*Time.deltaTime));
        if(!fishing)
		{
			if (controller.isGrounded) 
			{
	      		moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
           		moveDirection = transform.TransformDirection(moveDirection);
				moveDirection *= speed;
           		if (Input.GetButton("Jump"))
				{
                	moveDirection.y = jumpSpeed;
				}
			}
			moveDirection.y -= gravity * Time.deltaTime;
        	controller.Move(moveDirection * Time.deltaTime);
		}
			
		//if right mouse button is clicked
		if(Input.GetMouseButtonDown(0))
		{
			Screen.lockCursor = true;
				
			MouseLook mouse = GetComponent<MouseLook>();
			mouse.enabled = true;
				
			MouseLook camMouse = transform.FindChild("Camera").GetComponent<MouseLook>();
			camMouse.enabled = true;
				
		}
			
		if(Input.GetMouseButtonUp(0))
		{
			Screen.lockCursor = false;
				
			MouseLook mouse = GetComponent<MouseLook>();
			mouse.enabled = false;
				
			MouseLook camMouse = transform.FindChild("Camera").GetComponent<MouseLook>();
			camMouse.enabled = false;
		}
		
		if(Input.GetButtonDown("Run"))
		{
			speed = speed*2;			
		}
		
		if(Input.GetButtonUp("Run"))
		{
			speed = speed/2;
		}
	}
	
	public void inFishing(bool fish)
	{
		fishing = fish;
	}
}
