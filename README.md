# I Met a Little Girl (And She Liked to Fish)

This game was my first attemtp at a game jam game for Sophie Houlden's Fishing Jam and was the first game I'd ever made with a particular goal in mind. It was made over 7 days, in the evenings around university work and a part time job.

For my jam dev log see: http://jam.legendaryfisher.com/forum/viewtopic.php?f=3&t=103&sid=4df152efdd6d63d21163ad5ddfeddbf4

Here's how the game turned out:

![Alt Text](https://media.giphy.com/media/9V5gFDr8b4h0mKVPDg/giphy.gif)

At the time, it was an enjoyable project and a great challenge to try to build something under pressure. It's very basic, but I ended up implementing the UI elements, the player movement and the reticle. The idea of the game is that you're a little girl exploring this expansive landscape, fishing for massive sky fish (the flying blobs).

I polished the game over the week after that, but unfortunately I can't find the finished game! If I get some time, I'll search on one of my old laptops for it.

## Controls:
"WASD to move
Left click to move camera
Shift to run
Right Click on Flying Blob to fish
Space bar to jump when not fishing

When fishing, press space when the arrow is in the red to successfully tug a Blob fish closer. Pressing space in the yellow will be a failed tug, 3 of which will break the line. When the fish and line disappear (there's a small bug where the fish disappears but the line doesn't because it's the fish is too close to you) you have captured the fish at which point the menu at the top left will allow you to just keep going or restart the game."